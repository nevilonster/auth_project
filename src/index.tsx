import React from "react";
import { createRoot } from "react-dom/client";
import App from "App";
import GlobalStyles from "./globalStyles";

const container = document.getElementById("testReactApp");
const root = createRoot(container);
root.render(
  <React.StrictMode>
    <React.Suspense fallback={null}>
      <GlobalStyles />
      <App />
    </React.Suspense>
  </React.StrictMode>
);
