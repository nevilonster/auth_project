import { ContentWrap, TitlePageWrap } from "globalStyles/pages";
import { tableMock } from "mock/tableMock";
import SmartTable from "components/table/SmartTable";

const ViewerBoard = () => {
  return (
    <ContentWrap>
      <TitlePageWrap>Viewer</TitlePageWrap>
      <SmartTable data={tableMock} />
    </ContentWrap>
  );
};

export default ViewerBoard;
