import { ContentWrap, TitlePageWrap } from "globalStyles/pages";
import SmartTable from "components/table/SmartTable";
import { tableMock } from "mock/tableMock";

const Board = () => {
  return (
    <ContentWrap>
      <TitlePageWrap>Editor</TitlePageWrap>
      <SmartTable data={tableMock} />
    </ContentWrap>
  );
};

export default Board;
