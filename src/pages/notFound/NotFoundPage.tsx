import { useNavigate } from "react-router";
import styled from "styled-components";

import { ROUTES } from "router/enums";
import { StyledButton } from "globalStyles/theme/buttons";

const NotFoundPage = () => {
  const navigate = useNavigate();
  const onClickBack = () => {
    navigate(`${ROUTES.DASHBOARD}`, { replace: true });
  };

  return (
    <Wrap>
      <Title>Page Not Founded</Title>
      <StyledButton onClick={onClickBack}>Back to Dashboard</StyledButton>
    </Wrap>
  );
};

export default NotFoundPage;

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1rem;
  gap: 1rem;
`;

const Title = styled.div``;
