import { useForm } from "react-hook-form";
import styled from "styled-components";
import useToggle from "hooks/useToggle";
import { useNavigate } from "react-router-dom";
import Input from "components/input/Input";
import { hexToRgbA } from "utils/hexToRgbA";
import authApi from "services/api/authApi";
import { useLocalStorage } from "hooks/useLocalStorage";
import { ROUTES, TOKEN_KEY } from "router/enums";
import useUser from "users/context/useUser";
import { typography } from "globalStyles/theme/fonts";
import { useState } from "react";
import GlobalLoader from "components/loader/GlobalLoader";

export interface ILoginForm {
  email: string;
  password: string;
  rememberMe?: boolean;
}

const Login = () => {
  const { setItem } = useLocalStorage();
  const navigate = useNavigate();
  const user = useUser();
  const [isLoading, setIsLoading] = useState(false);

  const [isPasswordShown] = useToggle();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ILoginForm>();

  const onSubmit = async ({ email, password, rememberMe }) => {
    setIsLoading(true);
    await authApi
      .login({ email, password })
      .then(async response => {
        setItem(TOKEN_KEY, response.data);
        const userResponse = await user.loadMe();
        if (userResponse !== undefined) {
          navigate(`${ROUTES.DASHBOARD}`, { replace: true });
        }
      })
      .catch(() => {
        console.warn("error");
      })
      .finally(() => {
        console.warn("finally");
        setIsLoading(false);
      });
  };

  return (
    <Wrap>
      <Content>
        {isLoading && <GlobalLoader />}
        <form onSubmit={handleSubmit(onSubmit)}>
          <Label>
            <InputWrapper>
              <Input
                {...register("email", {
                  required: true,
                })}
                autoComplete="on"
                type="email"
                placeholder="Email"
                name="email"
              />
            </InputWrapper>
            <InputWrapper>
              <ErrorWrap>
                {errors.email && "Email is required and should be in a valid format"}
              </ErrorWrap>
            </InputWrapper>
          </Label>
          <Label>
            <InputWrapper>
              <Input
                {...register("password", {
                  required: true,
                })}
                autoComplete="on"
                type={isPasswordShown ? "text" : "password"}
                placeholder="Password"
                name="password"
              />
            </InputWrapper>
            <InputWrapper>
              <ErrorWrap>{errors.password && "Password is requeired field"}</ErrorWrap>
            </InputWrapper>
          </Label>
          <AdditionalWrap>
            <Checkbox>
              <input type="checkbox" {...register("rememberMe")} />
              Remember me
            </Checkbox>
            <ForgotPasswordWrap>Forgot password?</ForgotPasswordWrap>
          </AdditionalWrap>
          <StyledButton>Login</StyledButton>
        </form>
      </Content>
    </Wrap>
  );
};

export default Login;

const Wrap = styled.div`
  background-color: ${({ theme }) => hexToRgbA(theme.colors.bg.primary, 0.1)};
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ErrorWrap = styled.div`
  color: ${({ theme }) => theme.colors.error};
  ${({ theme }) => theme.typography.body};
`;

const Content = styled.div`
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15);
  border-radius: 0.25rem;
  display: flex;
  flex-direction: row;
  gap: 2px;
  padding: 2rem;
`;

const Label = styled.label`
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
  ${({ theme }) => theme.typography.body}
`;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;

  ${({ theme }) => theme.typography.body}
  i {
    position: absolute;
    height: 100%;
  }

  .icon-ic-eye {
    font-size: 2rem;
    cursor: pointer;
    right: 0.4rem;
    color: #5294c3;
  }

  .icon-ic-user,
  .icon-ic-lock {
    font-size: 1.5rem;
    top: calc((100% - 1.5rem) / 2);
    left: 0.4rem;
    color: black;
    opacity: 0.6;
  }
`;

const AdditionalWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const ForgotPasswordWrap = styled.div`
  width: 40%;
  cursor: pointer;
  text-decoration: underline;
  ${typography.smallText}
  color: ${({ theme }) => theme.colors.txt.primary};
  :hover {
    color: ${({ theme }) => {
      return hexToRgbA(theme.colors.txt.primary, 0.8);
    }};
  }
`;

const Checkbox = styled.label`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-bottom: 45px;
  color: black;
  opacity: 0.6;
  ${({ theme }) => theme.typography.smallText}
  line-height: 24px;
`;

const StyledButton = styled.button`
  width: 100%;
  height: 36px;
  background-color: ${({ theme }) => theme.colors.btn.primary};
  color: ${({ theme }) => theme.colors.bg.ternary};
  border: none;
  text-transform: uppercase;
`;
