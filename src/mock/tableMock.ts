export const tableMock = [
  { id: 1, name: "John Doe", age: 25 },
  { id: 2, name: "Jane Smith", age: 30 },
  { id: 3, name: "David Johnson", age: 28 },
  { id: 4, name: "Emily Davis", age: 32 },
];
