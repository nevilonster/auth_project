import React, { Suspense, useCallback, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { editorRoutes, IRoute, viewerRoutes } from "router/Configuration";
import Layout from "components/layout/Layout";
import RequireAuth from "router/RequireAuth";
import useUser from "users/context/useUser";
import { ROLES } from "router/enums";

interface IProps {
  routes: IRoute[];
}

const Router: React.FC<IProps> = ({ routes }) => {
  const [updater, setUpdater] = React.useState(0);
  const user = useUser();
  const getRoutes = useCallback(() => {
    return user.user && user.user.role === ROLES.EDITOR ? editorRoutes : viewerRoutes;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updater]);

  useEffect(() => {
    const subscribe = user.updater.subscribe(value => {
      setUpdater(value);
    });

    return () => {
      subscribe.unsubscribe();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Routes>
      {getRoutes().map((route, index) => {
        return (
          <Route
            path={route.path}
            key={index}
            element={
              <RequireAuth redirect={route.redirect} allowedRoles={route.allowedRoles}>
                {!route.element ? (
                  <div style={{ fontSize: 6, color: "gray" }}>NOT FOUND</div>
                ) : (
                  <Suspense fallback={route.fallback}>
                    {route.isLayout && <Layout>{route.element}</Layout>}
                    {!route.isLayout && route.element}
                  </Suspense>
                )}
              </RequireAuth>
            }
          />
        );
      })}
    </Routes>
  );
};

export default Router;
