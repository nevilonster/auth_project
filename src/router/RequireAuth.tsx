import { ROLES, ROUTES, TOKEN_KEY } from "./enums";
import { useLocation, useNavigate } from "react-router";
import { useEffect, useState } from "react";
import { useLocalStorage } from "hooks/useLocalStorage";
import EventDomDispatcher from "services/events/EventDomDispatcher";
import { LOGOUT_EVENT } from "services/events/consts";
import apiService from "services/api/apiService";
import GlobalLoader from "components/loader/GlobalLoader";
import useUser from "users/context/useUser";

export default function RequireAuth({
  children,
  redirect,
  allowedRoles = [],
}: {
  children: JSX.Element;
  redirect?: string;
  allowedRoles: ROLES[];
}) {
  const navigate = useNavigate();
  const location = useLocation();
  const [isReady, setIsReady] = useState(false);
  const { getItem } = useLocalStorage();
  const user = useUser();
  const { addEventListener, removeEventListener } = EventDomDispatcher();

  const logout = () => {
    console.warn("RequireAuth logout");
    window.localStorage.removeItem(TOKEN_KEY);
    user.logout();
    setIsReady(true);
    gotoLoginPage();
  };

  const gotoLoginPage = () => {
    navigate(`/${ROUTES.AUTH}`, { replace: true });
  };

  const checkAndGoToBoard = () => {
    if (location.pathname === `/${ROUTES.AUTH}`) {
      navigate(`${ROUTES.DASHBOARD}`, { replace: true });
    }
  };

  useEffect(() => {
    addEventListener(LOGOUT_EVENT, logout);
    return () => {
      removeEventListener(LOGOUT_EVENT, logout);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkUser = async () => {
    const response = await user.loadMe();
    if (response !== undefined) {
      if (user.user === null) {
        gotoLoginPage();
      } else {
        checkAndGoToBoard();
      }
      setIsReady(true);
    }
  };

  useEffect(() => {
    const token = getItem(TOKEN_KEY);
    if (!token) {
      localStorage.clear();
      setIsReady(true);
      gotoLoginPage();
    } else {
      checkUser();
    }
    return () => {
      const { cancelRequests } = apiService();
      cancelRequests();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isReady]);

  if (!isReady) {
    return <GlobalLoader />;
  }

  return children;
}
