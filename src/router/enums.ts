export enum ROUTES {
  DASHBOARD = "/",
  AUTH = "login",
  NOT_FOUND = "*",
}

export const TOKEN_KEY = "access_token";

export enum ROLES {
  VIEWER = "VIEWER",
  EDITOR = "EDITOR",
}
