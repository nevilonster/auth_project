import { ReactElement, ReactNode } from "react";
import { ROLES, ROUTES } from "./enums";
import Board from "pages/board/Board";
import NotFoundPage from "pages/notFound/NotFoundPage";
import Login from "pages/auth/Login";
import ViewerBoard from "pages/board/ViewerBoard";

export interface IRoute {
  path: string;
  fallback: NonNullable<ReactNode> | null;
  element: ReactElement;
  index?: boolean;
  redirect?: string;
  allowedRoles?: ROLES[];
  isLayout?: boolean;
}
const baseRoutes: IRoute[] = [
  {
    path: ROUTES.AUTH,
    element: <Login />,
    fallback: null,
  },
  {
    path: ROUTES.NOT_FOUND,
    element: <NotFoundPage />,
    fallback: null,
    isLayout: true,
  },
];
export const viewerRoutes: IRoute[] = [
  ...baseRoutes,
  {
    path: ROUTES.DASHBOARD,
    element: <ViewerBoard />,
    fallback: null,
    allowedRoles: [ROLES.VIEWER],
    redirect: ROUTES.AUTH,
    isLayout: true,
  },
];

export const editorRoutes: IRoute[] = [
  {
    path: ROUTES.AUTH,
    element: <Login />,
    fallback: null,
  },
  {
    path: ROUTES.DASHBOARD,
    element: <Board />,
    fallback: null,
    allowedRoles: [ROLES.EDITOR],
    redirect: ROUTES.AUTH,
    isLayout: true,
  },
  {
    path: ROUTES.NOT_FOUND,
    element: <NotFoundPage />,
    fallback: null,
    isLayout: true,
  },
];
