const cachedImages: { [url: string]: string } = {};

async function getCachedImage(url: string): Promise<string | null> {
  if (cachedImages[url]) {
    return cachedImages[url];
  }

  try {
    const cache = await caches.open("image-cache");
    const cachedResponse = await cache.match(url);

    if (cachedResponse) {
      const blob = await cachedResponse.blob();
      const imageUrl = URL.createObjectURL(blob);
      cachedImages[url] = imageUrl;
      return imageUrl;
    }
  } catch (error) {
    console.error("Error retrieving cached image:", error);
  }

  return null;
}

export async function loadImage(url: string): Promise<string> {
  const cachedImage = await getCachedImage(url);
  if (cachedImage) {
    return cachedImage;
  }

  try {
    const response = await fetch(url);
    const blob = await response.blob();
    const imageUrl = URL.createObjectURL(blob);
    cachedImages[url] = imageUrl;
    await cacheImage(url, blob);
    return imageUrl;
  } catch (error) {
    console.error("Error loading image:", error);
    throw error;
  }
}

async function cacheImage(url: string, imageBlob: Blob): Promise<void> {
  try {
    const cache = await caches.open("image-cache");
    const response = new Response(imageBlob);
    await cache.put(url, response);
  } catch (error) {
    console.error("Error caching image:", error);
  }
}
