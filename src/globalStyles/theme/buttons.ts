import styled, { css } from "styled-components";

const themeButtons = css`
  button {
    border-radius: 4px;
    height: 32px;
    font-family: "Poppins regular", sans-serif;
    font-size: 16px;
    line-height: 19px;
    outline: none;
    cursor: pointer;
    :hover {
      opacity: 0.9;
    }
  }
`;

export const StyledButton = styled.button`
  width: content-box;
  height: 36px;
  background-color: ${({ theme }) => theme.colors.btn.primary};
  color: ${({ theme }) => theme.colors.bg.secondary};
  border: none;
  text-transform: uppercase;
`;

export default themeButtons;
