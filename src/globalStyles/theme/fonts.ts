import PoppinsSemiBold from "fonts/Poppins-SemiBold.ttf";
import PoppinsRegular from "fonts/Poppins-Regular.ttf";
import PoppinsBold from "fonts/Poppins-Bold.ttf";
import { css } from "styled-components";

const fontsStyles = css`
  @font-face {
    font-family: "Poppins semibold";
    font-style: italic;
    src: url(${PoppinsSemiBold}) format("truetype");
  }
  @font-face {
    font-family: "Poppins regular";
    src: url(${PoppinsRegular}) format("truetype");
  }
  @font-face {
    font-family: "Poppins bold";
    src: url(${PoppinsBold}) format("truetype");
  }
`;

export const typography = {
  smallText: css`
    font-family: "Poppins regular", serif;
    font-size: 12px;
    line-height: 28px;
  `,
  body: css`
    font-family: "Poppins regular", serif;
    font-size: 18px;
    line-height: 32px;
  `,
  title: css`
    font-family: "Poppins bold", serif;
    font-size: 18px;
    line-height: 32px;
  `,
  buttonsTex: css`
    font-family: "Poppins regular", serif;
    font-size: 16px;
    line-height: 19px;
  `,
};

export default fontsStyles;
