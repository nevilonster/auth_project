import { createGlobalStyle } from "styled-components";
import fontsStyles, { typography } from "./theme/fonts";
import themeButtons from "./theme/buttons";
import themeInputs from "./theme/inputs";

const GlobalStyles = createGlobalStyle`
	#appRoot {
		${fontsStyles};
		${themeButtons};
		${themeInputs};
		${typography.body};
	}
`;

export default GlobalStyles;
