import { AuthContext } from "users/context/AuthContext";
import { useContext } from "react";

const useUser = () => {
  const { user } = useContext(AuthContext);
  return user;
};

export default useUser;
