import React, { createContext } from "react";
import UserModel from "users/admin/UserModel";
import axiosInstance from "services/api";
import initializeMockService from "services/api/mock/mockService";

type ComponentProps = {
  children: JSX.Element;
};

export type BaseAuthContext = {
  user: UserModel | null;
};

const userModel: UserModel = new UserModel();
initializeMockService(axiosInstance);
const AuthProvider = ({ children }: ComponentProps) => {
  return <AuthContext.Provider value={{ user: userModel }}>{children}</AuthContext.Provider>;
};

export const AuthContext = createContext<BaseAuthContext | null>(null);

export default AuthProvider;
