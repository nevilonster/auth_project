import authApi from "services/api/authApi";
import { IUser } from "users/admin/types";
import { BehaviorSubject } from "rxjs";

class UserModel {
  private _userData: IUser;
  private _updater: BehaviorSubject<number>;

  constructor() {
    this._updater = new BehaviorSubject<number>(0);
    console.warn("Model was initialized");
  }

  public async loadMe(): Promise<void> {
    const response = await authApi.me();
    this._userData = response?.data || null;
    this._updater.next(this._updater.value + 1);
    return response;
  }

  get user(): IUser {
    return this._userData;
  }

  get updater(): BehaviorSubject<number> {
    return this._updater;
  }

  public logout(): void {
    console.warn("logout");
    this._userData = null;
    this._updater.next(this._updater.value + 1);
  }
}

export default UserModel;
