import Router from "router";
import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter } from "react-router-dom";

import theme from "globalStyles/theme";
import { editorRoutes } from "router/Configuration";
import ToastMessages from "components/toasts/ToastMessages";
import AuthProvider from "users/context/AuthContext";

function App() {
  return (
    <Wrap id={"appRoot"}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <AuthProvider>
            <Router routes={editorRoutes} />
          </AuthProvider>
        </BrowserRouter>
        <ToastMessages />
      </ThemeProvider>
    </Wrap>
  );
}

export default App;

const Wrap = styled.div`
  width: 100vw;
  height: 100vh;
`;
