import apiService from "services/api/apiService";

export const AUTH_PATH = "/auth/login";
export const LOGOUT_PATH = "/auth/logout";
export const CURRENT_USER = "/users/current";

const { fetchApi, postApi } = apiService();
const authApi = {
  login: data => postApi(AUTH_PATH, data),
  me: () => fetchApi(CURRENT_USER),
};

export default authApi;
