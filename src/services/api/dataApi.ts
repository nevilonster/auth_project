import apiService from "services/api/apiService";

const { fetchApi } = apiService();
const TABLE_API_PATH = "/api/table";
const dataApi = {
  getData: () => fetchApi(TABLE_API_PATH),
};

export default dataApi;
