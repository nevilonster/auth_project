import axios, { AxiosError, AxiosResponse } from "axios";
import { ToastContentProps, toast, ToastPromiseParams } from "react-toastify";
import { NO_INTERNET_CONNECTION_MESSAGE } from "services/internetConnection/const";
import EventDomDispatcher from "services/events/EventDomDispatcher";
import ErrorToast from "components/toasts/ErrorToast";
import { LOGOUT_EVENT } from "services/events/consts";
import { TOKEN_KEY } from "router/enums";

export const getToken = () => {
  return window.localStorage.getItem(TOKEN_KEY);
};

export const getBaseURL = () => {
  const isLocalhost = window.location.hostname === "localhost";
  const DOMAIN_LENGTH = 2;
  const API_PREFIX = "";
  const SEPARATOR = "";
  const protocol = window.location.protocol + "//";
  const hostParts = window.location.host.split(SEPARATOR);
  const domain = hostParts.slice(hostParts.length - DOMAIN_LENGTH);
  const projectType = hostParts.slice(0, hostParts.length - DOMAIN_LENGTH);
  const baseURL = protocol + [...projectType, API_PREFIX, ...domain].join(SEPARATOR);
  const url = isLocalhost ? process.env.REACT_APP_API_URL : baseURL;
  return url;
};

const getBaseInstance = () => axios.create();

const makeInstance = () => {
  const baseInstance = getBaseInstance();
  baseInstance.interceptors.request.use(
    config => {
      config.baseURL = getBaseURL();
      config.headers = {
        "Content-type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      };
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );

  baseInstance.interceptors.response.use(
    response => {
      return response;
    },
    async function (error) {
      if (error.code === "ERR_CANCELED") return;
      if (!error.response) {
        return handlePromiseError(Promise.reject(error));
      }

      if (error.response.status === 401) {
        console.warn("logout");
        const { dispatchEvent } = EventDomDispatcher();
        dispatchEvent(new CustomEvent(LOGOUT_EVENT));
      }

      return handlePromiseError(Promise.reject(error));
    }
  );

  return baseInstance;
};

export default makeInstance();

export const handlePromiseError = <Handler extends (params) => string>(
  response: Promise<AxiosResponse>,
  errorSchemaHandler?: Handler
) => {
  const handlers: ToastPromiseParams = {
    error: {
      render: (
        props: ToastContentProps<AxiosError<{ error: Record<string, string[]>; title: string }>>
      ) => {
        if (props.data?.code === "ERR_NETWORK") {
          return NO_INTERNET_CONNECTION_MESSAGE;
        }
        const status = props.data.response.status;
        if (status >= 500) return "The server is unavailable at the moment";
        if (status >= 401) return "You are not authorized to perform this action";
        return <ErrorToast description={"Something went wrong..."} />;
      },
    },
  };
  toast.promise(response, handlers);
};
