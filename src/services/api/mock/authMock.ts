import { ROLES } from "router/enums";

const users = {
  "1": {
    id: "1",
    first_name: "John",
    last_name: "Doe",
    email: "john.doe@gmail.com",
    username: "john.doe",
    role: ROLES.VIEWER,
  },
  "2": {
    id: "2",
    first_name: "Alice",
    last_name: "Parker",
    email: "alice.parker@gmail.com",
    username: "alice.parker",
    role: ROLES.EDITOR,
  },
};

export const authMock = {
  "john.doe@gmail.com": {
    pass: "Viewer1234!",
    access_token: "123456789",
  },
  "alice.parker@gmail.com": {
    pass: "Editor1234!",
    access_token: "987654321",
  },
};

export const authMockUsers = {
  "123456789": users["1"],
  "987654321": users["2"],
};
