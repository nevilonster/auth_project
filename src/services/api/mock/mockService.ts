import MockAdapter from "axios-mock-adapter";
import { AUTH_PATH, CURRENT_USER } from "services/api/authApi";
import { authMock, authMockUsers } from "services/api/mock/authMock";
import { isDemoMode } from "services/api/const";
import { AxiosInstance } from "axios";

let mock: MockAdapter;

const runMockService = () => {
  mock.onPost(AUTH_PATH).reply(data => {
    const { email, password } = JSON.parse(data.data);
    if (authMock[email] && password === authMock[email].pass) {
      return [200, authMock[email].access_token];
    }
    return [401, { message: "Invalid credentials" }];
  });
  mock.onGet(CURRENT_USER).reply(data => {
    const token = data.headers.Authorization.split("Bearer ")[1];
    if (authMockUsers[token]) {
      return [200, authMockUsers[token]];
    }
    return [401, { message: "Invalid credentials" }];
  });
};

const initializeMockService = (axios: AxiosInstance) => {
  console.log("initializeMockService");
  if (!mock && isDemoMode) {
    mock = new MockAdapter(axios, { delayResponse: 500 });
    runMockService();
  }
};

export default initializeMockService;
