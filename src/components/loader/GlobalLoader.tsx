import styled from "styled-components";
import LoaderSpinner from "components/loader/LoaderSpinner";
import { typography } from "globalStyles/theme/fonts";

const GlobalLoader = () => {
  return (
    <Inner>
      <LoaderSpinner />
      <LoadingTitle>Loading</LoadingTitle>
    </Inner>
  );
};

export default GlobalLoader;

const Inner = styled.div`
  top: 0;
  left: 0;
  position: absolute;
  display: flex;
  box-sizing: border-box;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.2);
  animation: tp-fade-in 0.5s forwards ease;
  z-index: 4;
`;

const LoadingTitle = styled.div`
  ${typography.body};
  width: 100%;
  text-align: center;
  margin-top: 20px;
`;
