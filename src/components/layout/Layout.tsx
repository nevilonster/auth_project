import React from "react";
import styled from "styled-components";
import Header from "components/header/Header";

type Props = {
  children?: React.ReactNode;
};

const Layout: React.FC<Props> = ({ children }) => {
  return (
    <Container>
      <HeaderContainer>
        <Header />
      </HeaderContainer>
      <ContentContainer>
        <Content>{children}</Content>
      </ContentContainer>
    </Container>
  );
};

export default Layout;

const HEADER_HEIGHT = 70;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
`;

const HeaderContainer = styled.div`
  height: ${HEADER_HEIGHT}px;
`;

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1rem;
  border: 1px solid rgba(0, 0, 0, 0.12);
  border-radius: 4px;
`;

const Content = styled.div`
  height: calc(100vh - ${HEADER_HEIGHT}px - 2rem);
  min-height: 650px;
  width: 100%;
  display: flex;
  overflow: hidden;
`;
