import React, { useState } from "react";
import styled from "styled-components";

interface IComponentProps {
  data: { id: number; name: string; age: number }[];
}

interface ISortConfig {
  key: string;
  direction: "asc" | "desc";
}

const SmartTable = ({ data }: IComponentProps) => {
  const [sortConfig, setSortConfig] = useState<ISortConfig>({ key: "", direction: "asc" });
  const handleSort = (key: string) => {
    let direction: "asc" | "desc" = "asc";
    if (sortConfig.key === key && sortConfig.direction === "asc") {
      direction = "desc";
    }
    setSortConfig({ key, direction });
  };

  const sortedData = [...data].sort((a, b) => {
    if (a[sortConfig.key] < b[sortConfig.key]) {
      return sortConfig.direction === "asc" ? -1 : 1;
    }
    if (a[sortConfig.key] > b[sortConfig.key]) {
      return sortConfig.direction === "asc" ? 1 : -1;
    }
    return 0;
  });

  return (
    <ResponsiveTableWrapper>
      <ResponsiveTable>
        <thead>
          <tr>
            <TableHeader onClick={() => handleSort("id")}>ID</TableHeader>
            <TableHeader onClick={() => handleSort("name")}>Name</TableHeader>
            <TableHeader onClick={() => handleSort("age")}>Age</TableHeader>
          </tr>
        </thead>
        <tbody>
          {sortedData.map(item => (
            <TableRow key={item.id}>
              <ResponsiveTableCell>{item.id}</ResponsiveTableCell>
              <ResponsiveTableCell>{item.name}</ResponsiveTableCell>
              <ResponsiveTableCell>{item.age}</ResponsiveTableCell>
            </TableRow>
          ))}
        </tbody>
      </ResponsiveTable>
    </ResponsiveTableWrapper>
  );
};

export default SmartTable;

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

const TableHeader = styled.th`
  cursor: pointer;
  padding: 8px;
  border-bottom: 1px solid #ddd;
`;

const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f2f2f2;
  }
`;

const TableCell = styled.td`
  padding: 8px;
  text-align: left;
`;

const ResponsiveTableWrapper = styled.div`
  overflow-x: auto;
`;

const ResponsiveTable = styled(StyledTable)`
  @media (max-width: 768px) {
    font-size: 12px;
  }
`;

const ResponsiveTableCell = styled(TableCell)`
  @media (max-width: 768px) {
    padding: 4px;
  }
`;
