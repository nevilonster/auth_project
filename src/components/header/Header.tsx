import React from "react";
import styled from "styled-components";
import { typography } from "globalStyles/theme/fonts";
import useUser from "users/context/useUser";
import { StyledButton } from "globalStyles/theme/buttons";
import EventDomDispatcher from "services/events/EventDomDispatcher";
import { LOGOUT_EVENT } from "services/events/consts";

const Header = () => {
  const user = useUser();
  const onLogout = () => {
    const { dispatchEvent } = EventDomDispatcher();
    dispatchEvent(new CustomEvent(LOGOUT_EVENT));
  };
  return (
    <Container>
      <TitleDetailWrap>
        {user.user.role}: {user.user.first_name} {user.user.last_name}
      </TitleDetailWrap>
      <StyledButton onClick={onLogout}>Logout</StyledButton>
    </Container>
  );
};

export default Header;

const TitleDetailWrap = styled.div`
  ${typography.title}
  color: ${({ theme }) => theme.colors.bg.primaryTextColor};
  align-self: center;
`;

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 1rem;
  height: 100%;
  background: ${({ theme }) => theme.colors.bg.primary};
  box-shadow: 0 2px 4px rgba(36, 36, 36, 0.16);
`;
