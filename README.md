#### please use

```
"engines": {
    "npm": ">=9.6.2",
    "node": ">=v18.0.0"
},
```

### install dependencies:

```npm i```

### run project:

```npm run local```

## Credentials: 
```
viewer: "john.doe@gmail.com": {
pass: "Viewer1234!",
```
```
editor: "alice.parker@gmail.com":
pass: "Editor1234!",
```
